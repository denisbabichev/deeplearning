import numpy as np

print("operations with lists in Python:")

a = [1, 2, 3]
b = [4, 5, 6]

print("a+b:", a + b)

try:
    print(a * b)
except TypeError:
    print("a*b no sense in Python")

print()

print("operations from module numpy:")

a = np.array([1, 2, 3])
b = np.array([4, 5, 6])

print("a+b:", a + b)

print("a*b:", a * b)

a = np.array([[1, 2, 3],
              [4, 5, 6]])

print('a:\n')

print(a)

print('a.sum(axis=0):', a.sum(axis=0))

print('a.sum(axis=1):', a.sum(axis=1))

b = np.array([10, 20, 30])
print("a+b:\n", a + b, )
